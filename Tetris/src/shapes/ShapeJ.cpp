#include "ShapeJ.h"

ShapeJ::ShapeJ(Game& game, SDL_Renderer* &r, Keyboard* &key) :Shape(game, r, key) {

	for (int y = 0; y < form.size(); y++) {
		for (int x = 0; x < form.size(); x++) {
			form[y][x] = nullptr;
		}
	}

	form[0][1] = new Tile(r, "res/PinkTile.png");//not blue one, blues are for Is
	form[1][1] = new Tile(r, "res/PinkTile.png");
	form[2][1] = new Tile(r, "res/PinkTile.png");
	form[2][0] = new Tile(r, "res/PinkTile.png");

	form[0][1]->setPosition(Vector2D(1, 0));
	form[1][1]->setPosition(Vector2D(1, 1));
	form[2][1]->setPosition(Vector2D(1, 2));
	form[2][0]->setPosition(Vector2D(0, 2));

	tiles.push_back(form[0][1]);
	tiles.push_back(form[1][1]);
	tiles.push_back(form[2][1]);
	tiles.push_back(form[2][0]);

	position = Vector2D(4, 0);

}
