#ifndef SHAPE_H
#define SHAPE_H

#include <cmath>
#include <array>
#include <vector>

#include "tiles/Tile.h"
#include "../Game.h"
#include "../gfx/SpriteSheet.h"
#include "../physics/Vector2D.h"
//#include "../input/Keyboard.h"

class Shape {
public:
	Shape(Game& game, SDL_Renderer* &r, Keyboard* &key);

	virtual void tick();
	virtual void render();

	virtual void rotate();
	void moveLeft();
	void moveRight();

	void startFalling();
	void fall();
	void drop();//is is not falling -> drop down 1 tile, otherwise -> accelerate

	std::array<std::array<Tile*, 4>, 4> getForm();
	Vector2D getPosition();
	Vector2D getSpeed();
	void setPosition(Vector2D pos);

protected:
	std::array<std::array<Tile*, 4>, 4> form;//3x3 or 2x2 in derived classes?
	std::vector<Tile*> tiles;
	int rotation;
	bool isFalling;
	Vector2D position, speed;

	Game& game;
	SDL_Renderer* &r;
	Keyboard* &key;
	
};

#endif SHAPE_H
