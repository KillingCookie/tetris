#include "ShapeL.h"

ShapeL::ShapeL(Game& game, SDL_Renderer* &r, Keyboard* &key) :Shape(game, r, key) {

	for (int y = 0; y < form.size(); y++) {
		for (int x = 0; x < form.size(); x++) {

			form[y][x] = nullptr;

		}

	}

	form[0][0] = new Tile(r, "res/OrangeTile.png");//not blue one, blues are for Is
	form[1][0] = new Tile(r, "res/OrangeTile.png");
	form[2][0] = new Tile(r, "res/OrangeTile.png");
	form[2][1] = new Tile(r, "res/OrangeTile.png");

	form[0][0]->setPosition(Vector2D(0, 0));
	form[1][0]->setPosition(Vector2D(0, 1));
	form[2][0]->setPosition(Vector2D(0, 2));
	form[2][1]->setPosition(Vector2D(1, 2));

	tiles.push_back(form[0][0]);
	tiles.push_back(form[1][0]);
	tiles.push_back(form[2][0]);
	tiles.push_back(form[2][1]);

	position = Vector2D(4, 0);
}
