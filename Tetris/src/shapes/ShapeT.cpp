#include "ShapeT.h"

ShapeT::ShapeT(Game& game, SDL_Renderer* &r, Keyboard* &key) :Shape(game, r, key) {

	for (int y = 0; y < form.size(); y++) {
		for (int x = 0; x < form.size(); x++) {

			form[y][x] = nullptr;

		}

	}

	form[0][0] = new Tile(r, "res/PurpleTile.png");//not blue one, blues are for Is
	form[0][1] = new Tile(r, "res/PurpleTile.png");
	form[0][2] = new Tile(r, "res/PurpleTile.png");
	form[1][1] = new Tile(r, "res/PurpleTile.png");

	form[0][0]->setPosition(Vector2D(0, 0));
	form[0][1]->setPosition(Vector2D(1, 0));
	form[0][2]->setPosition(Vector2D(2, 0));
	form[1][1]->setPosition(Vector2D(1, 1));

	tiles.push_back(form[0][0]);
	tiles.push_back(form[0][1]);
	tiles.push_back(form[0][2]);
	tiles.push_back(form[1][1]);

}
