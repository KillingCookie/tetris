#include "ShapeI.h"

ShapeI::ShapeI(Game& game, SDL_Renderer* &r, Keyboard* &key):Shape(game, r, key) {

	for (int y = 0; y < form.size(); y++) {
		for (int x = 0; x < form.size(); x++) {

			if (x == 0) {

				form[y][x] = new Tile(r, "res/BlueTile.png");
				form[y][x]->setPosition(Vector2D(x, y));
				tiles.push_back(form[y][x]);

			}else {

				form[y][x] = nullptr;

			}

		}
		
	}
	position = Vector2D(4, 0);
}

void ShapeI::rotate() {
	std::array<std::array<Tile*, 4>, 4> copy;
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			copy[y][x] = form[y][x];
		}
	}
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			form[x][y] = copy[y][x];
		}
	}
}
