#include "Shape.h"

Shape::Shape(Game& game, SDL_Renderer* &r, Keyboard* &key): game(game), r(r), key(key) {

	rotation = 0;
	isFalling = false;
	
	speed = Vector2D(0, 0);
	//setposition in subclasses
	//surf = imgLoad.load(path);

	//tex = SDL_CreateTextureFromSurface(r, surf);//error check

}

void Shape::tick() {
	//for form update tile position
	for (int y = 0; y < form.size(); y++) {
		for (int x = 0; x < form.size(); x++) {
			if (form[y][x]) {
				form[y][x]->setPosition(Vector2D(position.x + x, position.y + y));
			}
		}
	}

	position += speed;//when position is > 24 -> delete an object

}

void Shape::render() {

	//if (position.x > 0) {

	for (Tile*& t : tiles) {
		t->render();
	}

	//}

}

void Shape::rotate() {

	std::array<std::array<Tile*, 4>, 4> copy;
	for (int y = 0; y < 3; y++) {
		for (int x = 0; x < 3; x++) {
			copy[y][x] = form[y][x];
		}
	}
	for (int y = 0; y < 3; y++) {
		for (int x = 0; x < 3; x++) {
			form[x][2 - y] = copy[y][x];
		}
	}
}

void Shape::moveLeft() {

	position.x -= 1;

}

void Shape::moveRight() {

	position.x += 1;

}

void Shape::startFalling() {

	isFalling = true;
	position = Vector2D(4, 0);
	speed = Vector2D(0, (float)1 / (60 / 2));

}

void Shape::fall() {

	position.y = floor(position.y + speed.y);
	speed = Vector2D(0, 0);
	tick();
	isFalling = false;

}

void Shape::drop() {

	if (isFalling) {
		speed *= Vector2D(0, 5);
		isFalling = false;
	}

}

std::array<std::array<Tile*, 4>, 4> Shape::getForm() {

	return this->form;

}

Vector2D Shape::getPosition() {

	return this->position;

}

void Shape::setPosition(Vector2D pos) {

	this->position = pos;
	for (int y = 0; y < form.size(); y++) {
		for (int x = 0; x < form.size(); x++) {
			if (form[y][x]) {
				form[y][x]->setPosition(Vector2D(position.x + x, position.y + y));
			}
		}
	}
}

Vector2D Shape::getSpeed() {

	return this->speed;

}
