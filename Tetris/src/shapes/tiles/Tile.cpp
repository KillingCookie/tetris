#include "Tile.h"

Tile::Tile(SDL_Renderer* &r, const char* path):r(r) {

	ImageLoader imgLoad = ImageLoader();
	tex = imgLoad.load(path, r);

}

Tile::~Tile() {
	SDL_DestroyTexture(tex);
}

void Tile::tick() {

	position += speed;

}

void Tile::render() {

	//if (position.x > 0) { WHY ITS HERE????

		SDL_Rect dest;
		dest.x = floor(position.x)*25;
		dest.y = (position.y - 4)*25;
		dest.w = 25;
		dest.h = 25;

		SDL_RenderCopy(r, tex, NULL, &dest);

	//}

}

void Tile::setPosition(Vector2D pos) {

	this->position = pos;

}

void Tile::setSpeed(Vector2D speed) {

	this->speed = speed;

}

Vector2D Tile::getPosition() {

	return position;

}

Vector2D Tile::getSpeed() {

	return speed;

}
