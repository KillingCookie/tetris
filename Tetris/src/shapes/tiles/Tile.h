#ifndef TILE_H
#define TILE_H

#include "SDL.h"

#include "../../physics/Vector2D.h"
#include "../../gfx/ImageLoader.h"

class Tile {
public:
	Tile(SDL_Renderer* &r, const char* path);
	~Tile();

	void tick();
	void render();

	void setPosition(Vector2D pos);
	void setSpeed(Vector2D speed);
	Vector2D getPosition();
	Vector2D getSpeed();

private:
	Vector2D position, speed;
	SDL_Renderer* &r;
	SDL_Texture *tex;

};

#endif
