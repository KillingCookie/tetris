#ifndef SHAPEI_H
#define SHAPEI_H

#include "Shape.h"

class ShapeI : protected Shape {// for now we gonna draw textures directrly clipping spritesheet inside render() in shape class - no!!!
public:
	ShapeI(Game& game, SDL_Renderer* &r, Keyboard* &key);

private:
	void rotate() override;
};

#endif SHAPEI_H
