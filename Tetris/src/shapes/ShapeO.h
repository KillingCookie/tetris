#ifndef SHAPEO_H
#define SHAPEO_H

#include "Shape.h"

class ShapeO : protected Shape {
public:
	ShapeO(Game& game, SDL_Renderer* &r, Keyboard* &key);

private:
	void rotate() override;
};

#endif SHAPEO_H
