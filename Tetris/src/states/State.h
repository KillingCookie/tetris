#ifndef STATE_H
#define STATE_H

#include <vector>

#include "SDL.h"

#include "../Game.h"
#include "../input/Keyboard.h"

class State {
public:
	State(Game& game, SDL_Renderer* &r, Keyboard* &key);

	virtual void tick() = 0;
	virtual void render() = 0;

protected:
	Game& game;

	SDL_Renderer* &r;
	Keyboard* &key;

private:
	std::vector<State*> states;

};

#endif STATE_H
