#include "GameState.h"

GameState::GameState(Game& game, SDL_Renderer* &r, Keyboard* &key) : State(game, r, key) {

	map = new Map(game, r, key);
	points = 0;

}

void GameState::tick() {
	//add pause
	map->tick();

}

void GameState::render() {

	map->render();

}