#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "State.h"
#include "../map/Map.h"

class GameState : public State {
public:
	GameState(Game& game, SDL_Renderer* &r, Keyboard* &key);

	void tick();
	void render();

private:
	Map *map;
	int points;

};

#endif GAMESTATE_H
