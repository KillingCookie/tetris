#include "Map.h"
#include "../shapes/Shape.h"
#include "../shapes/ShapeI.h"
#include "../shapes/ShapeJ.h"
#include "../shapes/ShapeL.h"
#include "../shapes/ShapeO.h"
#include "../shapes/ShapeS.h"
#include "../shapes/ShapeT.h"
#include "../shapes/ShapeZ.h"

Map::Map(Game& game, SDL_Renderer* &r, Keyboard* &key):game(game),r(r),key(key) {

	if (TTF_Init() == 0)
		std::cout << "sdl init successfull...\n";

	ImageLoader img = ImageLoader();
	tex = img.load("res/Map.png", r);

	for (auto i : field)
		for (auto j : i)
			j = nullptr;

	mapTile = new Tile(r, "res/MapTile.png");

	srand(time(NULL));
	for (int i = 0; i < 4; i++) {
		nextShapes.push(getRandomShape());
	}
	currentShape = nextShapes.front();
	nextShapes.pop();
	nextShapes.push(getRandomShape());
	currentShape->startFalling();
	nextShapes.front()->setPosition(Vector2D(11, 6));
	
}

void Map::tick() {

	//check for lose
	for (int y = 0; y < 4; y++) {
		for (auto x : field[y]) {
			if (x) {
				endgame();
			}
		}
	}

	//check for full rows
	for (int y = 0; y < field.size(); y++) {
		int full = 0;
		for (auto x : field[y]) {
			if (x) {
				full++;
			}
		}if (full == field[y].size()) {
			destroyRow(y);
		}
	}
	//bounds
	int bounds[2][4][2];
	for (int i = 0; i < currentShape->getForm().size(); i++) {
		int boundX[2]{-1, -1};// < 0 ---> empty row/column
		int boundY[2]{ -1, -1 };
		for (int j = 0; j < currentShape->getForm().size(); j++) {
			if (currentShape->getForm()[i][j]) {
				if (boundX[0] < 0) {
					boundX[0] = j;
					boundX[1] = j;
				}
				else {
					boundX[1] = j;
				}
			}
			if (currentShape->getForm()[j][i]) {
				if (boundY[0] < 0) {
					boundY[0] = j;
					boundY[1] = j;
				}
				else {
					boundY[1] = j;
				}
			}
		}
		bounds[0][i][0] = boundX[0];
		bounds[0][i][1] = boundX[1];
		bounds[1][i][0] = boundY[0];
		bounds[1][i][1] = boundY[1];
	}
	//moving right-left
	if (key->left && canMoveLeft(bounds)) {// + other tiles around
		currentShape->moveLeft();
	}
	else if (key->right && canMoveRight(bounds)) {// && last right one
		currentShape->moveRight();
	}
	else if (key->down) {
		currentShape->drop();
	}
	else if (key->up) {
		rotate();
	}
	//falling
	for (int x = 0; x < currentShape->getForm().size(); x++) {
		if (bounds[1][x][1] >= 0) {
			if (floor((currentShape->getPosition()).y + (currentShape->getSpeed()).y) + bounds[1][x][1] == field.size() - 1) {
				fall();
				return;
			}
			else if (field[floor((currentShape->getPosition()).y + (currentShape->getSpeed()).y) + bounds[1][x][1] + 1][currentShape->getPosition().x + x]) {//if another shape is below you\\//x part??
				float a = floor((currentShape->getPosition()).y + (currentShape->getSpeed()).y) + bounds[1][x][1];
				fall();
				return;
			}
		}
	}currentShape->tick();
}

bool Map::canMoveLeft(int bounds[2][4][2]) {

	for (int i = 0; i < 4; i++) {
		if (bounds[0][i][0] >= 0) {
			if (currentShape->getPosition().x + bounds[0][i][0] - 1 < 0) {
				return false;
			}
			else if (field[floor((currentShape->getPosition()).y) + i][(currentShape->getPosition()).x + bounds[0][i][0] - 1] 
				|| field[ceil((currentShape->getPosition()).y) + i][(currentShape->getPosition()).x + bounds[0][i][0] - 1]) {
				return false;
			}
		}
	}return true;

}

bool Map::canMoveRight(int bounds[2][4][2]) {
	
	for (int i = 0; i < 4; i++) {
		if (bounds[0][i][1] >= 0) {
			if (currentShape->getPosition().x + bounds[0][i][1] + 1 > field[i].size() - 1) {
				return false;
			}
			else if (field[floor((currentShape->getPosition()).y) + i][(currentShape->getPosition()).x + bounds[0][i][1] + 1]
				|| field[ceil((currentShape->getPosition()).y) + i][(currentShape->getPosition()).x + bounds[0][i][1] + 1]) {
				return false;
			}
		}
	}
	return true;
}

void Map::rotate() {

	currentShape->rotate();
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			if (floor(currentShape->getPosition().y + i >= 24 || ceil(currentShape->getPosition().y + i >= 24))) {
				continue;
			}
			else if (currentShape->getPosition().x + j > field[i].size() - 1) {
				if (currentShape->getForm()[i][j]) {
					currentShape->rotate();
					currentShape->rotate();
					currentShape->rotate();
					return;
				}
				else {
					continue;
				}
			}
			else if (currentShape->getPosition().x + j < 0) {
				if (currentShape->getForm()[i][j]) {
					currentShape->rotate();
					currentShape->rotate();
					currentShape->rotate();
					return;
				}
				else {
					continue;
				}
			}
			else if ((field[floor(currentShape->getPosition().y + i)][currentShape->getPosition().x + j] && currentShape->getForm()[i][j]) ||
				(field[ceil(currentShape->getPosition().y + i)][currentShape->getPosition().x + j] && currentShape->getForm()[i][j])) {
				currentShape->rotate();
				currentShape->rotate();
				currentShape->rotate();
				return;
			}	
		}
	}
}

void Map::destroyRow(int row) {

	for (Tile* i : field[row]) {
		delete i;
		i = nullptr;
	}
	for (int i = row-1; i > 0; i--) {
		for (int j = 0; j < 10; j++) {
			field[i + 1][j] = field[i][j];
			if (field[i + 1][j]) {
				field[i + 1][j]->setPosition(Vector2D(field[i + 1][j]->getPosition().x, field[i + 1][j]->getPosition().y + 1));
			}
		}
	}
	destroyedRows++;
}

void Map::endgame() {
	exit(0);
}

void Map::fall() {

	currentShape->fall();
	for (int x = 0; x < currentShape->getForm().size(); x++) {
		for (int y = 0; y < currentShape->getForm().size(); y++) {
			if (currentShape->getForm()[y][x]) {
				field[(currentShape->getPosition()).y + y][(currentShape->getPosition()).x + x] = currentShape->getForm()[y][x];
			}
		}
	}delete currentShape;

	currentShape = nextShapes.front();
	nextShapes.pop();
	nextShapes.push(getRandomShape());
	currentShape->startFalling();
	nextShapes.front()->setPosition(Vector2D(11, 6));
}

void Map::render() {

	for (int j = 4; j < field.size(); j++) {
		for (int i = 0; i < field[j].size(); i++) {
			mapTile->setPosition(Vector2D(i, j));
			mapTile->render();
		}
	}

	for (int y = 0; y < field.size(); y++) {
		for (Tile* x : field[y]) {
			if (x) {
				x->render();
			}
		}
	}
	currentShape->render();
	nextShapes.front()->render();

	TTF_Font* Sans = TTF_OpenFont("fonts/OpenSans-Regular.ttf", 24); 
	SDL_Color White = { 255, 255, 255 }; 

	SDL_Surface* nextShapeMessageSurf = TTF_RenderText_Solid(Sans, "Next shape", White);
	SDL_Surface* rowsMessageSurf = TTF_RenderText_Solid(Sans, "Destroyed rows", White); 
	SDL_Surface* destroyedRowsMessageSurf = TTF_RenderText_Solid(Sans, std::to_string(destroyedRows).c_str(), White);
	SDL_Texture* nextShapeMessageTex = SDL_CreateTextureFromSurface(r, nextShapeMessageSurf);
	SDL_Texture* rowsMessageTex = SDL_CreateTextureFromSurface(r, rowsMessageSurf); 
	SDL_Texture* destroyedRowsMessageTex = SDL_CreateTextureFromSurface(r, destroyedRowsMessageSurf);

	SDL_Rect Message_rect; 
	Message_rect.x = 260;  
	Message_rect.y = 10; 
	Message_rect.w = 100; 
	Message_rect.h = 25; 
	SDL_RenderCopy(r, nextShapeMessageTex, NULL, &Message_rect); 

	Message_rect.x = 260;  
	Message_rect.y = 25*17; 
	Message_rect.w = 100; 
	Message_rect.h = 25; 
	SDL_RenderCopy(r, rowsMessageTex, NULL, &Message_rect);

	Message_rect.x = 260 + 50 - destroyedRowsMessageSurf->w/2;
	Message_rect.y = 25*18; 
	Message_rect.w = destroyedRowsMessageSurf->w;
	Message_rect.h = 25; 
	SDL_RenderCopy(r, destroyedRowsMessageTex, NULL, &Message_rect);

	SDL_FreeSurface(nextShapeMessageSurf);
	SDL_FreeSurface(rowsMessageSurf);
	SDL_FreeSurface(destroyedRowsMessageSurf);
	SDL_DestroyTexture(nextShapeMessageTex);
	SDL_DestroyTexture(rowsMessageTex);
	SDL_DestroyTexture(destroyedRowsMessageTex);

	TTF_CloseFont(Sans);
}

Shape* Map::getRandomShape() {
	switch (rand() % 7) {

	case I_SHAPE:
		return (Shape*) new ShapeI(game, r, key);
		break;
	case J_SHAPE:
		return (Shape*) new ShapeJ(game, r, key);
		break;
	case L_SHAPE:
		return (Shape*) new ShapeL(game, r, key);
		break;
	case O_SHAPE:
		return (Shape*) new ShapeO(game, r, key);
		break;
	case S_SHAPE:
		return (Shape*) new ShapeS(game, r, key);
		break;
	case T_SHAPE:
		return (Shape*) new ShapeT(game, r, key);
		break;
	case Z_SHAPE:
		return (Shape*) new ShapeZ(game, r, key);
		break;
	default:
		return nullptr;
		break;
	}
}
