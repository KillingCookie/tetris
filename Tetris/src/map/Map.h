#ifndef MAP_H
#define MAP_H

#include <array>
#include <vector>
#include <queue>
#include <map>
#include <cstdlib>
#include <ctime>

//#include "../shapes/Shape.h"
#include "../gfx/ImageLoader.h"
#include "../Game.h"
#include "../input/Keyboard.h"
#include "SDL_ttf.h"

class Shape;
class Tile;

class Map {
public:
	Map(Game& game, SDL_Renderer* &r, Keyboard* &key);

	void tick();
	void render();

	bool canMoveLeft(int bounds[2][4][2]);
	bool canMoveRight(int bounds[2][4][2]);
	void rotate();
	void fall();
	void destroyRow(int row);
	void endgame();

	Shape* getRandomShape();
	
private:

	enum ShapeNum {
		I_SHAPE, J_SHAPE, L_SHAPE, O_SHAPE, S_SHAPE, T_SHAPE, Z_SHAPE
	};

	Shape* currentShape;//but can you rotate it
	std::queue<Shape*> nextShapes;
	std::array<std::array<Tile*, 10>, 24> field;//24?
	Tile* mapTile;
	unsigned int destroyedRows{ 0 };
	
	SDL_Texture* tex;
	SDL_Renderer* &r;
	Game& game;
	Keyboard* &key;

};

#endif MAP_H
