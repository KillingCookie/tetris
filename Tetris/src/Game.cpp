#include "Game.h"
#include "states\State.h"
#include "states\GameState.h"
#include "input\Keyboard.h"

Game::Game(){

	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
		std::cout << "sdl init successfull...\n";

	if (win = SDL_CreateWindow("Tetris", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 375, 500, SDL_WINDOW_SHOWN))
		std::cout << "window created...\n";

	if (r = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC))
		std::cout << "renderer created...\n";

	isRunning = true;
	std::cout << "game running..." << std::endl;
	
	//fps = 60;

	key = new Keyboard(this);
	currentState = (State*) new GameState(*this, r, key);

}
	
Game::~Game() {

	delete key;
	delete currentState;

	SDL_DestroyWindow(win);
	SDL_DestroyRenderer(r);
	SDL_Quit();

}

void Game::tick() {

	currentState->tick();

}

void Game::render() {

	SDL_RenderClear(r);

	currentState->render();

	SDL_RenderPresent(r);

}

void Game::eventLoop() {

	/*while (SDL_PollEvent(&e) != 0){

		if (e.type == SDL_QUIT)
			isRunning = true;
		
	}*/

	key->tick();

	SDL_PollEvent(&e);

	switch (e.type) {
	case SDL_QUIT:
		isRunning = false;
		break;

	default:
		break;

	}

}

int Game::run() {

	const int FPS = 60;
	const int frameDelay = 1000 / FPS;

	Uint32 frameStart;
	int frameTime;

	while (isRunning) {

		frameStart = SDL_GetTicks();

		this->eventLoop();
		this->tick();
		this->render();

		frameTime = SDL_GetTicks() - frameStart;

		if (frameTime < frameDelay)
			SDL_Delay(frameDelay - frameTime);

	}

	return 0;

}

/*void Game::setFPS(int fps) {

	this->fps = fps;

}*/

SDL_Event* Game::getEvent() {

	return &this->e;

}
