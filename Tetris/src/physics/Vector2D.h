#ifndef VECTOR2D_H
#define VECTOR2D_H

class Vector2D {
public:

	float x, y;

	Vector2D();
	Vector2D(float x, float y);

	friend Vector2D& operator+(Vector2D& v1, Vector2D& v2);
	friend Vector2D& operator-(Vector2D& v1, Vector2D& v2);
	friend Vector2D& operator*(Vector2D& v1, Vector2D& v2);
	friend Vector2D& operator/(Vector2D& v1, Vector2D& v2);

	Vector2D& operator+=(Vector2D& v);
	Vector2D& operator-=(Vector2D& v);
	Vector2D& operator*=(Vector2D& v);
	Vector2D& operator/=(Vector2D& v);

};

#endif VECTOR2D_H
