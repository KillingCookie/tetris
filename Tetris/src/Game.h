#ifndef GAME_H
#define GAME_H

#include <iostream>

#include "SDL.h"

//#include "states\State.h"
//#include "states\GameState.h"
//#include "input\Keyboard.h"

class State;
class GameState;
class Keyboard;

class Game{
public:
	Game();

	~Game();

	void tick();
	void render();
	void eventLoop();
	int run();

	//void setFPS(int fps);

	SDL_Event* getEvent();

private:
	//int fps;

	SDL_Window * win;
	SDL_Renderer * r;
	SDL_Event e;
	Keyboard* key;

	State* currentState;

	bool isRunning = false;

};

#endif GAME_H