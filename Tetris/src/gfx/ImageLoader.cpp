#include "ImageLoader.h"

ImageLoader::ImageLoader(){

	IMG_Init(IMG_INIT_PNG);

}

SDL_Texture* ImageLoader::load(const char* path, SDL_Renderer* r) {

	SDL_Surface* surf = IMG_Load(path);//error check!??		return IMG_Load(path);
	SDL_Texture* tex = SDL_CreateTextureFromSurface(r, surf);
	SDL_FreeSurface(surf);

	return tex;

}
