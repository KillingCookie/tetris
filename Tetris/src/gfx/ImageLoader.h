#ifndef IMAGELOADER_H
#define IMAGELOADER_H

#include <SDL_image.h>

//#include "SpriteSheet.h"

class ImageLoader{
private:

public:
	ImageLoader();
	//~ImageLoader();
	SDL_Texture* load(const char* path, SDL_Renderer* r);//SDL_Surface* load(const char* path);

};

#endif IMAGELOADER_H