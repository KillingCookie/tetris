#ifndef SPRITESHEET_H
#define SPRITESHEET_H

#include <string>

#include <SDL.h>

#include "ImageLoader.h"
//#include "../Game.h"

class SpriteSheet {//may be usefull for future cropping of the surfaces
public:
	SDL_Texture* getTexture(std::string path, int x, int y, int w, int h, SDL_Renderer* &e);
	
private:
	SDL_Surface* sheet;
	SDL_Renderer* &e;

};

#endif SPRITESHEET_H
