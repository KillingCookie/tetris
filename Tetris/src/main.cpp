#include <iostream>

#include "Game.h"

int main(int argc, char * argv[]) {

	Game *game = new Game();

	int exitCode = game->run();
	
	delete game;
	return exitCode;

}
