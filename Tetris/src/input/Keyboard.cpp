#include "Keyboard.h"

Keyboard::Keyboard(Game* game):up(keys[SDLK_UP % 256]),down(keys[SDLK_DOWN % 256]),left(keys[SDLK_LEFT % 256]),right(keys[SDLK_RIGHT % 256]){

	this->game = game;
	this->e = game->getEvent();

	/*up = keys[SDLK_UP % 256];//or w?
	down = keys[SDLK_DOWN % 256];//or s?
	left = keys[SDLK_LEFT % 256];
	right = keys[SDLK_RIGHT % 256];*/

}

void Keyboard::pressed() {

	if (e->type == SDL_KEYDOWN) {
		switch (e->key.keysym.sym) {

		case SDLK_UP:
			if (keysReleased[SDLK_UP % 256]) {
				keysPressed[SDLK_UP % 256] = true;
				keysReleased[SDLK_UP % 256] = false;
			}
			break;
		case SDLK_DOWN:
			if (keysReleased[SDLK_DOWN % 256]) {
				keysPressed[SDLK_DOWN % 256] = true;
				keysReleased[SDLK_DOWN % 256] = false;
			}
			break;
		case SDLK_LEFT:
			if (keysReleased[SDLK_LEFT % 256]) {
				keysPressed[SDLK_LEFT % 256] = true;
				keysReleased[SDLK_LEFT % 256] = false;
			}
			break;
		case SDLK_RIGHT:
			if (keysReleased[SDLK_RIGHT % 256]) {
				keysPressed[SDLK_RIGHT % 256] = true;
				keysReleased[SDLK_RIGHT % 256] = false;
			}
			break;
		default:
			break;

		}
	}

}

void Keyboard::released() {

	if (e->type == SDL_KEYUP) {
		switch (e->key.keysym.sym) {

		case SDLK_UP:
			keysPressed[SDLK_UP % 256] = false;
			keysReleased[SDLK_UP % 256] = true;
			break;
		case SDLK_DOWN:
			keysPressed[SDLK_DOWN % 256] = false;
			keysReleased[SDLK_DOWN % 256] = true;
			break;
		case SDLK_LEFT:
			keysPressed[SDLK_LEFT % 256] = false;
			keysReleased[SDLK_LEFT % 256] = true;
			break;
		case SDLK_RIGHT:
			keysPressed[SDLK_RIGHT % 256] = false;
			keysReleased[SDLK_RIGHT % 256] = true;
			break;
		default:
			break;

		}
	}

}

void Keyboard::tick() {

	SDL_PollEvent(e);

	for (int i = 0; i < 256; i++) {
		if (!keysReleased[i]) {
			keysPressed[i] = false;
		}
	}

	pressed();

	for (int i = 0; i < 256; i++) {
		keys[i] = keysPressed[i];
	}

	released();

}
